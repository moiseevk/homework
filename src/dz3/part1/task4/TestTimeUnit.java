package dz3.part1.task4;

public class TestTimeUnit {
    public static void main(String[] args) {
        TimeUnit time1 = new TimeUnit(23,7,59);
        TimeUnit time2 =  new TimeUnit(8);

        time1.getTime();
        time1.setTime(3,15,25);
        time1.getTime();

        time2.getTime();
        time2.getAmPmTime();
        time2.setTime(10, 25, 34);
        time2.getAmPmTime();

    }

}
