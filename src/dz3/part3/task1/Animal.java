package dz3.part3.task1;

public abstract class Animal {

    protected final void eat() {
        System.out.println("Ест");
    }

    protected final void sleep() {
        System.out.println("Спит");
    }

    protected abstract void wayOfBirth();
}
