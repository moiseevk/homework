package dz3.part3.task1;

public class Dolphin extends Mammal implements Swimming {

    @Override
    public void swim() {
        System.out.println("Плавает быстро");
    }

}
