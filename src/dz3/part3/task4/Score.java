package dz3.part3.task4;

public class Score {
    private double averageScore; //

    public Score(int numberOfScoreDog) {
        this.averageScore = Participant.score.get(numberOfScoreDog - 1);
    }

    public double getScore() {
        return averageScore;
    }

    public void setScore(double averageScore) {
        this.averageScore = averageScore;

    }
}
