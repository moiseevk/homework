package professional.dz3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Class<APrinter> clazz = APrinter.class;
        try {
            Method method = clazz.getDeclaredMethod("print", int.class);
            method.setAccessible(true);
            method.invoke(clazz.getDeclaredConstructor().newInstance(), 3);
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException | InstantiationException | IllegalArgumentException ex) {
            System.out.println("Неверные данные");
        }
    }
}
