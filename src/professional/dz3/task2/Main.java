package professional.dz3.task2;

import professional.dz3.task1.IsLike;

public class Main {
    public static void main(String[] args) throws IllegalAccessException {

        Class<ClassWithIsLikeAnnotation> clazz1 = ClassWithIsLikeAnnotation.class;
        checkAnnotation(clazz1);

        Class<ClassWithoutIsLikeAnnotation> clazz2 = ClassWithoutIsLikeAnnotation.class;
        checkAnnotation(clazz2);
    }

    public static void checkAnnotation(Class<?> clazz) {
        if (clazz.isAnnotationPresent(IsLike.class)) {
            System.out.println(clazz.getAnnotation(IsLike.class));
        }
        else {
            System.out.println("Такой аннотации нет");
        }
    }
}
