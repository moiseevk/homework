package professional.dz3.dop2;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        System.out.println(checkString(s));
    }

    public static boolean checkString(String s) {
        Deque<Character> dequeue = new LinkedList<>();

        char[] array = s.toCharArray();

        for (char ch : array) {
            if (ch == '{' || ch == '[' || ch == '(') {
                dequeue.addFirst(ch);
            }
            else {
                if (!dequeue.isEmpty() && ((dequeue.getFirst() == '{' && ch == '}') || (dequeue.getFirst() == '[' && ch == ']') || (dequeue.getFirst() == '(' && ch == ')'))) {
                    dequeue.removeFirst();
                }
                else {
                    return false;
                }
            }
        }
        return dequeue.isEmpty();
    }
}
