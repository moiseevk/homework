package professional.dz3.dop1;

import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        checkString(s);

    }

    public static void checkString(String s) {
        int count = 0;
        if (s.length() == 0) {
            System.out.println(true);
        }
        else {
            for (int i = 0; i < s.length(); i++) {
                if (count < 0) {
                    break;
                }
                if (s.charAt(i) == '(') {
                    count++;
                }
                else if (s.charAt(i) == ')') {
                    count--;
                }
            }
            if (count == 0) {
                System.out.println(true);
            }
            else {
                System.out.println(false);
            }

        }
    }
}