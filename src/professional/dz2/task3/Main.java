package professional.dz2.task3;

import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1= new TreeSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        Set<Integer> set2 = new TreeSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);
        PowerfulSet powerfulSet1 = new PowerfulSet();
        Set<Integer> result1 = powerfulSet1.intersection(set1, set2);
        System.out.println(result1);

        Set<Integer> set3= new TreeSet<>();
        set3.add(1);
        set3.add(2);
        set3.add(3);
        Set<Integer> set4 = new TreeSet<>();
        set4.add(0);
        set4.add(1);
        set4.add(2);
        set4.add(4);
        PowerfulSet powerfulSet2 = new PowerfulSet();
        Set<Integer> result2 = powerfulSet2.union(set3, set4);
        System.out.println(result2);

        Set<Integer> set5 = new TreeSet<>();
        set5.add(1);
        set5.add(2);
        set5.add(3);
        Set<Integer> set6 = new TreeSet<>();
        set6.add(0);
        set6.add(1);
        set6.add(2);
        set6.add(4);
        PowerfulSet powerfulSet3 = new PowerfulSet();
        Set<Integer> result3 = powerfulSet3.relativeComplement(set5, set6);
        System.out.println(result3);
    }
}
