package professional.dz2.task4;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
public class Document {
    private int id;
    private String name;
    private int pageCount;


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> documentMap = new HashMap<>();
        for (int i = 0; i < documents.size(); i++) {
            documentMap.put(documents.get(i).id, documents.get(i));
        }
        return documentMap;
    }

    @Override
    public String toString() {
        return "Название документа: " + getName() + ", количество страниц: " + getPageCount();
    }
}
