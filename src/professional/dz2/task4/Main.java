package professional.dz2.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        ArrayList<Document> list = new ArrayList<>();
        Document document1 = new Document();
        document1.setId(1);
        document1.setName("News1");
        document1.setPageCount(100);

        Document document2 = new Document();
        document2.setId(2);
        document2.setName("News2");
        document2.setPageCount(200);

        Document document3 = new Document();
        document3.setId(3);
        document3.setName("News3");
        document3.setPageCount(300);

        list.add(document1);
        list.add(document2);
        list.add(document3);

        Map<Integer, Document> documents = Document.organizeDocuments(list);

        // Проверка на работоспособность поиска в HashMap по id
        System.out.println(documents.get(1));
        System.out.println(documents.get(2));
        System.out.println(documents.get(3));
    }
}
