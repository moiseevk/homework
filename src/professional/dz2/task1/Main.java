package professional.dz2.task1;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(3);
        list.add(3);

        Set<Integer> set = uniqueElements(list);
        System.out.println(set);
    }

    public static <T> Set<T> uniqueElements(ArrayList<T> list) {
        Set<T> set = new TreeSet<>();

        for(T elem : list) {
            set.add(elem);
        }
        return set;
    }
}
