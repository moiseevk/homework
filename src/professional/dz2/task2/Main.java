package professional.dz2.task2;

import java.util.Arrays;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        String t = scan.nextLine();

        char[] s1 = s.toUpperCase().toCharArray();
        char[] t1 = t.toUpperCase().toCharArray();
        Arrays.sort(s1);
        Arrays.sort(t1);

        int counter = 0;
        if (s1.length == t1.length) {
            for (int i = 0; i < s1.length; i++) {
                if (s1[i] == t1[i]) {
                    counter++;
                }
            }
            if (counter == s1.length) {
                System.out.println(true);
            }
            else {
                System.out.println(false);
            }
        }
        else {
            System.out.println(false);
        }
    }
}
