package professional.dz2.dop;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] array = {"the", "day", "is", "sunny", "the", "the", "the",
                "sunny", "is", "is", "day"};
        int k = 4;

        String[] result = popularWords(array, k);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }
    }

    /**
     * Метод, который принимает массив words и целое положительное число k.
     *
     * @param words Массив слов
     * @param k     Количество слов, которые надо вывести.
     * @return
     */
    public static String[] popularWords(String[] words, int k) {
        String[] result = new String[k];
        Map<String, Integer> map = new TreeMap<>();
        List<String> keys = new ArrayList<>();
        List<Integer> values = new ArrayList<>();

        // Заполняем map с данными ключ(cлово) - значение(кол-во повторений этого слова)
        for (int i = 0; i < words.length; i++) {
            int count = 0;
            for (int j = 0; j < words.length; j++) {
                if (words[i].equals(words[j])) {
                    count++;
                }
            }
            map.put(words[i], count);
        }

        // Заполняем массивы значениями
        for (Map.Entry<String, Integer> elem : map.entrySet()) {
            String key = elem.getKey();
            Integer value = elem.getValue();
            keys.add(key);
            values.add(value);
        }
        System.out.println(keys); // Для отображения, что наш массив заполнился словами
        System.out.println(values); // Для отображения, что наш массив заполнился значениями

        // Вложенные циклы для сортировки слов по уменьшению кол-ва повторений их в массиве
        for (int i = 0; i < values.size() - 1; i++) {
            for (int j = i + 1; j < values.size(); j++) {
                if (values.get(i) < values.get(j)) {
                    Integer temp = values.get(i);
                    values.set(i, values.get(j));
                    values.set(j, temp);
                    String temp1 = keys.get(i);
                    keys.set(i, keys.get(j));
                    keys.set(j, temp1);
                } else if (values.get(i) == values.get(j)) {
                    if (keys.get(i).compareTo(keys.get(j)) < 0) {
                        Integer temp1 = values.get(i);
                        values.set(i, values.get(j));
                        values.set(j, temp1);
                    }
                }
                if (i == values.size() - 2) {
                    result[i] = keys.get(i);
                    result[values.size() - 1] = keys.get(keys.size() - 1);
                } else {
                    result[i] = keys.get(i);
                }
            }
        }
        return result;
    }
}
