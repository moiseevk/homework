package professional.dz4.task5;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        list.stream()
                .map(str -> str.toUpperCase())
                .forEach(str -> {
                    String lastStr = list.get(list.size() - 1).toUpperCase();
                        if (!str.equals(lastStr)) {
                            System.out.println(str + ",");
                        }
                        else {
                            System.out.println(str + ".");
                        }
                });
    }
}
