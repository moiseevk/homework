package professional.dz4.task4;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List.of(1.3, 1.5, 1.4, 1.2, 1.6, 1.9, 2.0, 1.7, 1.1, 1.8).stream()
                .sorted((lhs, rhs) -> {
                  if (lhs < rhs) {
                      return 1;
                  }
                  if (lhs > rhs) {
                      return -1;
                  }
                  else {
                      return 0;
                  }
                })
                .forEach(digit -> System.out.println(digit));
    }
}
