package professional.dz4.task6;

import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Set<Integer> set1 = Set.of(4, 5, 6);
        Set<Integer> set2 = Set.of(8, 9, 3);
        Set<Integer> set3 = Set.of(1, 0, 10);
        Set<Integer> set4 = Set.of(11, 12, 13);
        Set<Integer> set5 = Set.of(14, 15, 16);

        Set<Set<Integer>> bigSet = Set.of(set1, set2, set3, set4, set5);
        Set<Integer> newSet = bigSet.stream()
                .flatMap(Set::stream)
               .collect(Collectors.toSet());

        System.out.println(newSet);
    }
}
