package professional.dz4.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

       int sum =  list.stream()
                .filter(digit -> digit % 2 == 0)
                .reduce(0, (a, b) -> a + b);

        System.out.println(sum);
    }
}
