package professional.dz4.task3;

import java.util.List;
public class Main {
    public static void main(String[] args) {
        long count = List.of("abc", "", "", "def", "qqq").stream()
                .filter(str -> str.length() > 0)
                .count();

        System.out.println(count);
    }
}
