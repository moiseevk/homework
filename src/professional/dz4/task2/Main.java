package professional.dz4.task2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        int amount = List.of(1, 2, 3, 4, 5).stream()
                .reduce(1, (a, b) -> a * b);

        System.out.println(amount);
    }
}
