package professional.dz6.dop1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println("Является ли число " + n + " числом Армстронга? Ответ: " + checkArmstrongNumber(n));

    }

    public static boolean checkArmstrongNumber(int number) {
        String numberString = String.valueOf(number);
        int sum = 0;

        for (int i = numberString.length() - 1; i >= 0; i--) {
            String ch = String.valueOf(numberString.charAt(i));
            sum += Math.pow(Integer.parseInt(ch), numberString.length());
        }
        return sum == number;
    }
    }
