package professional.dz6.dop2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println("Является ли число " + n + " простым? Ответ: " + checkPrimeNumber(n));
    }

    public static boolean checkPrimeNumber(int number) {
        int count = 0;
        if ( number != 1 && number % number == 0 && number % 1 == 0) {
            for (int i = 2; i <= number - 1; i++) {
                if (number % i == 0) {
                    count++;
                }
            }
        }
        else {
            return false;
        }
        return count == 0;
    }
}
