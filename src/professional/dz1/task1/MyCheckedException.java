package professional.dz1.task1;

public class MyCheckedException extends Exception {
    public MyCheckedException() {
        super();
    }
    public MyCheckedException(String message) {
        super(message);
    }

}
