package professional.dz1.task4;

public class NotEvenException extends Exception {

    public NotEvenException() {
        super();
    }

    public NotEvenException(String message) {
        super(message);
    }
}
