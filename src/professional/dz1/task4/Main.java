package professional.dz1.task4;

public class Main {
    public static void main(String[] args) {

        try {
            MyEvenNumber myEvenNumber1 = new MyEvenNumber(2);
            MyEvenNumber myEvenNumber2 = new MyEvenNumber(9);
        }
        catch (NotEvenException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
