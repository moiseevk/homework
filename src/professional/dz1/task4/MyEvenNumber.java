package professional.dz1.task4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws NotEvenException {
        if (n % 2 == 0) {
            this.n = n;
        }
        else {
            throw new NotEvenException("Вы вводите нечётное число! Вы ввели: " + n);
        }
    }
    public int getN() {
        return n;
    }
}
