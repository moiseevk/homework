package professional.dz1.task2;

public class MyUncheckedException extends RuntimeException {

    public MyUncheckedException() {
        super();
    }

    public MyUncheckedException(String message) {
        super(message);
    }

}
