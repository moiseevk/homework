package professional.dz1.task6;

public class InvalidDateException extends Exception {

    public InvalidDateException() {
        super("Неверно указана дата!");
    }
    public InvalidDateException(String message) {
        super(message);
    }
}
