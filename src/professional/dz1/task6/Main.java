package professional.dz1.task6;

public class Main {
    public static void main(String[] args) {
        try {
            FormValidator.checkName("Ivan");
        } catch(InvalidNameException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            FormValidator.checkBirthDate("31.12.1900");
        } catch (InvalidDateException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            FormValidator.checkGender("MALE");
        } catch (InvalidGenderException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            FormValidator.checkHeight("185");
        } catch (InvalidHeightException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
