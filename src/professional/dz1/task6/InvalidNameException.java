package professional.dz1.task6;

public class InvalidNameException extends Exception {

    public InvalidNameException() {
        super("Неверно указано имя");
    }

    public InvalidNameException(String message) {
        super(message);
    }
}
