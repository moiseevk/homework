package professional.dz1.task6;

public class InvalidGenderException extends Exception {

    public InvalidGenderException() {
        super("Неверно указан пол");
    }

    public InvalidGenderException(String message) {
        super(message);
    }
}
