package professional.dz1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;
public class ReadAndWrite {
    private static final String DIRECTORY = "C:\\Users\\компьютер\\IdeaProjects\\SberDz\\src\\professional\\dz1\\task3";
    private static final String OUTPUT = "output.txt";
    private static final String INPUT = "input.txt";

    public static void main(String[] args) {
        try {
            ReadAndWriteInFile();
        } catch (IOException ex) {
            System.out.println("Произошла ошибка, так как не был найден файл");
        }
    }

    public static void ReadAndWriteInFile() throws IOException {
        Scanner scan = new Scanner(new File(DIRECTORY + "\\" + INPUT));
        Writer writer = new FileWriter(DIRECTORY + "\\" + OUTPUT);
        try(scan; writer) {
            while(scan.hasNext()) {
                writer.write(scan.nextLine().toUpperCase() + "\n");
            }
        }

    }
}
