package DZTWOFirstPart;

import java.util.Scanner;

/*
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы
● строчные латинские символы
● числа
● специальные знаки(_*-)
 */
public class TaskDop1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите длину желаемого пароля: ");
        int n = scan.nextInt(); // Считываем длину желаемого пароля

            generateRandomPassword(n);


    }
    /**
     * Метод, который проверяет количество символов желаемого пароля. Если количество символов больше или равно 8, то метод генерирует пароль, состоящий из заглавных латинских символов, строчных латинских символов, чисел и специальных знаков(_*-).
     *
     * @param n длина желаемого пароля
     */
    public static void generateRandomPassword(int n) {
        Scanner scan = new Scanner(System.in);
        String password = "";
        boolean isTrueOrFalse = false;
        boolean count1 = false;
        boolean count2 = false;
        boolean count3 = false;
        boolean count4 = false;
        String[] arr = {"abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789", "_*-"};
        do {
            if (n >= 8) { // Тут генерируем случайный пароль, который удовлетворяет условиям
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < 1; j++) {
                        int random = (int)(Math.random() * arr.length);
                        if (random == 0) {
                            char c = arr[0].charAt((int)(Math.random() * arr[0].length()));
                            password += c;
                            count1 = true;
                        }
                        else if (random == 1) {
                            char c = arr[1].charAt((int)(Math.random() * arr[1].length()));
                            password += c;
                            count2 = true;
                        }
                        else if (random == 2) {
                            char c = arr[2].charAt((int)(Math.random() * arr[2].length()));
                            password += c;
                            count3 = true;
                        }
                        else if (random == 3) {
                            char c = arr[3].charAt((int) (Math.random() * arr[3].length()));
                            password += c;
                            count4 = true;
                        }
                        else {
                            char c = arr[random].charAt((int)(Math.random() * arr[random].length()));
                            password += c;
                        }
                    }
                    if (i == n - 1) {
                        if (count1 && count2 && count3 && count4) {
                            System.out.println(password);
                            isTrueOrFalse = true;
                        }
                        else {
                            i = 0;
                            password = "";
                            count1 = false;
                            count2 = false;
                            count3 = false;
                            count4 = false;
                        }
                    }
                    }

                }
                else { // Если пароль оказался меньше 8 символов, то пишем, что такое количество символов является небезопасным и необходимо заново ввести длину желаемого пароля
                System.out.println("Пароль с " + n + " количеством символов небезопасен");
                System.out.println("Введите длину желаемого пароля: ");
                n = scan.nextInt(); // Считываем длину желаемого пароля
            }
        } while (!isTrueOrFalse) ;
    }
}
