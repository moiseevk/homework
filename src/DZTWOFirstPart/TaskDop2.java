package DZTWOFirstPart;

import java.util.Arrays;
import java.util.Scanner;

/*Решить задачу 7 основного дз за линейное время
        На вход подается число N — длина массива. Затем передается массив
        целых чисел (ai) из N элементов, отсортированный по возрастанию.
        Необходимо создать массив, полученный из исходного, возведением в квадрат
        каждого элемента, упорядочить элементы по возрастанию и вывести их на
        экран
        Входные данные
        6
        -10 -5 1 3 3 8
        Выходные данные
        1 9 9 25 64 100
        2
        -7 7
        Выходные данные
        49 49

 */
public class TaskDop2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }
        Arrays.sort(arr);
        int pol = 0; // Заводим счётчик положительных элементов в массиве
        int otr = 0; // Заводим счётчик отрицательных элементов в массиве
        for (int i = 0; i < arr.length; i++) { // Считываем кол-во отрицательных и положительных элементов в массиве
            if (arr[i] >= 0) {
                pol++;
            } else {
                otr++;
            }
        }

        int[] arrOtr = new int[otr]; // Объявляем переменную, в которой будем хранить отрицательные элементы цикла arr. Длина массива равняется количеству отрицательных элементов в массиве arr.
        int[] arrPol = new int[pol];// Объявляем переменную, в которой будем хранить положительных элементы цикла arr. Длина массива равняется количеству отрицательных элементов в массиве arr.

        split(arr, arrOtr, arrPol);
        squareArrays(arrOtr,arrPol);
        int[] sortArrOtr = sortArrOtr(arrOtr);
        mergeTroArrays(sortArrOtr, arrPol);
    }

    /**
     * Метод разделяет массив на два массива. В одном массиве будут храниться только отрицательные элементы исходного массива, а в другом только положительные элементы исходного массива.
     * @param arr Исходный массив
     * @param arrOtr Новый массив с только отрицательными элементами
     * @param arrPol Новый массив с только положительными элементами
     */
    public static void split(int[] arr, int[] arrOtr, int[] arrPol) {
        int n = 0;
        int p = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 0) {
                arrPol[n++] = arr[i];
            }
            else {
                arrOtr[p++] = arr[i];
            }
        }
    }

    /**
     * Метод возводит элементы каждого массива в квадрат
     * @param arr1 Первый массив
     * @param arr2 Второй массив
     */
    public static void squareArrays(int[] arr1, int[] arr2) {
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = (int) Math.pow(arr1[i], 2);
        }

        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = (int) Math.pow(arr2[i], 2);
        }
    }

    /**
     * Метод сортирует элементы отрицательного массива, которые были возведены в квадрат по возрастанию. Сортировка необходимо из-за того, что если число является отрицательным и чем оно меньше другого отрицательного числа, то тем больше будет квадрат этого числа, чем квадрат другого отрицательного числа, который в первой степени был больше.
     * Яркий пример это числа -9 и - 8. В первой степени -8 больше, чем -9, но возведя в квадрат данные числа, мы увидим, что -9 больше, чем -8, так как 81 больше 64.
     * @param arr1 Массив с положительными числами, которые до возведения в квадрат были отрицательными
     * @return возвращает отсортированный массив по возрастанию
     */
    public static int[] sortArrOtr(int[] arr1) {
        int[] sortArrOtr = new int[arr1.length];
        int k = arr1.length - 1;

        for (int i = 0; i < sortArrOtr.length; i++) {
            sortArrOtr[i] = arr1[k--];
        }
        return sortArrOtr;
    }

    /**
     * Метод объединяет 2 отсортированных массива.
     * @param arr1 Первый массив
     * @param arr2 Второй массив
     */
    public static void mergeTroArrays(int[] arr1, int[] arr2) {
        int[] mergeArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;

        //Обход двух массивов
        while(i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergeArray[k++] = arr1[i++];
            } else {
                mergeArray[k++] = arr2[j++];
            }
        }
        //Сохраняем оставшиеся элементы отрицательного массива
        while(i < arr1.length) {
            mergeArray[k++] = arr1[i++];
        }

        //Сохраняем оставшиеся элементы положительного массива
        while(j < arr2.length) {
            mergeArray[k++] = arr2[j++];
        }
        for (int g : mergeArray) {
            System.out.print(g + " ");
        }

    }

}
